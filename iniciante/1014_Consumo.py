km_percorrido = int(input())
combustivel_gasto = float(input())
consumo_medio = km_percorrido / combustivel_gasto

print('{:.3f} km/l'.format(consumo_medio))