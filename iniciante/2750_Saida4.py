n1 = 0
n2 = 1
n3 = 2
n4 = 3
n5 = 4
n6 = 5
n7 = 6
n8 = 7
n9 = 8
n10 = 9
n11 = 10
n12 = 11
n13 = 12
n14 = 13
n15 = 14
n16 = 15

oct1 = 0
oct2 = 1
oct3 = 2
oct4 = 3
oct5 = 4
oct6 = 5
oct7 = 6
oct8 = 7
oct9 = 10
oct10 = 11
oct11 = 12
oct12 = 13
oct13 = 14
oct14 = 15
oct15 = 16
oct16 = 17

hex1 = 0
hex2 = 1
hex3 = 2
hex4 = 3
hex5 = 4
hex6 = 5
hex7 = 6
hex8 = 7
hex9 = 8
hex10 = 9
hex11 = 'A'
hex12 = 'B'
hex13 = 'C'
hex14 = 'D'
hex15 = 'E'
hex16 = 'F'

print('---------------------------------------')
print('| Decimal   |  Octal  |  Hexadecimal  |')
print('---------------------------------------')
print('|      {}   |    {}   |       {}         |'.format(n1, oct1, hex1))
print('|      {}   |    {}   |       {}         |'.format(n2, oct2, hex2))
print('|      {}   |    {}   |       {}         |'.format(n3, oct3, hex3))
print('|      {}   |    {}   |       {}         |'.format(n4, oct4, hex4))
print('|      {}   |    {}   |       {}         |'.format(n5, oct5, hex5))
print('|      {}   |    {}   |       {}         |'.format(n6, oct6, hex6))
print('|      {}   |    {}   |       {}         |'.format(n7, oct7, hex7))
print('|      {}   |    {}  |       {}          |'.format(n8, oct8, hex8))
print('|      {}   |    {}   |       {}       |'.format(n9, oct9, hex9))
print('|      {}   |    {}   |       {}       |'.format(n10, oct10, hex10))
print('|      {}   |    {}   |       {}       |'.format(n11, oct11, hex11))
print('|      {}   |    {}   |       {}       |'.format(n12, oct12, hex12))
print('|      {}   |    {}   |       {}       |'.format(n13, oct13, hex13))
print('|      {}   |    {}   |       {}       |'.format(n14, oct14, hex14))
print('|      {}   |    {}   |       {}       |'.format(n15, oct15, hex15))
print('|      {}   |    {}   |       {}       |'.format(n16, oct16, hex16))
print('---------------------------------------')
